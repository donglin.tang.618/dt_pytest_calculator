import pytest
from calculator import calculator
#2 cases that brings ValueException and 1 case that is normal
def test_inputError():
     with pytest.raises(Exception) as excinfo:   
        calculator("str1",1,"/")
     assert str(excinfo.value)
     with pytest.raises(Exception) as excinfo:   
        calculator("9",1,"/")
     assert str(excinfo.value)
     with pytest.raises(Exception) as excinfo:   
        calculator(9,1,"/")
     assert str(excinfo.value)
def test_divisionBy0():
    calculator(10,0,"/")
    
